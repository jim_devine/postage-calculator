package edu.pitt.cs1635.jmd149.prog1;

/*James Devine
 * CS 1635 Project 1
 * Postage Calculator
 * 1/20/2014
 */

import java.text.DecimalFormat;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class MainActivity extends Activity {

	int type;
	Double cost;
	TextView resultTextView;
	int count = 0;
	EditText getInput;
	DecimalFormat formatter = new DecimalFormat("#0.00"); 
	RadioButton rb1, rb2, rb3;
	String result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Sets up the tabs
		TabHost th = (TabHost) findViewById(R.id.tabhost);
		th.setup();		

		TabSpec specs = th.newTabSpec("tag1");
		specs.setContent(R.id.tab1);
		specs.setIndicator("Postage Calculator");
		th.addTab(specs);

		specs = th.newTabSpec("tag2");
		specs.setContent(R.id.tab2);
		specs.setIndicator("Letter");
		th.addTab(specs);

		specs = th.newTabSpec("tag3");
		specs.setContent(R.id.tab3);
		specs.setIndicator("Large Envelope");
		th.addTab(specs);

		specs = th.newTabSpec("tag4");
		specs.setContent(R.id.tab4);
		specs.setIndicator("Package");
		th.addTab(specs);
		//done setting up tabs
		
		//setting background color to grey
		RelativeLayout re = (RelativeLayout) findViewById(R.id.re);
		re.setBackgroundColor(Color.GRAY);

		//find the ids of the essential views
		rb1 = (RadioButton) findViewById(R.id.rb1);
		rb2 = (RadioButton) findViewById(R.id.rb2);
		rb3 = (RadioButton) findViewById(R.id.rb3);
		getInput = (EditText) findViewById(R.id.editText1);
		resultTextView = (TextView) findViewById(R.id.textView2);
		Button ok = (Button) findViewById(R.id.bOk);

		//onClickListener for the calculate button
		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String s = getInput.getText().toString(); //reads the input

				//checks for empty input
				if (s.matches("")) {
					result = "Please enter weight in ounces";
					resultTextView.setText(result);

				} else {

					Double d = Double.parseDouble(s); //parses the input from string to double
					Double roundUp = Math.ceil(d); //rounds the decimal up
					
					//determines the shipping method using type as the identifier
					if (rb1.isChecked())
						type = 1;
					else if (rb2.isChecked())
						type = 2;
					else if (rb3.isChecked())
						type = 3;

					//checks for valid weights
					if (d <= 0) {
						result = "Please enter a valid weight.";
						resultTextView.setText(result);

					} else if (d > 13) {
						result = "Weight must be less than 13 ounces!";
						resultTextView.setText(result);

					}
					
					//handles the requested shipping method and applies specific shipping rules
					else {
						if (type == 1) {
							if (d > 3.5){
								result = "Too heavy, must use Large Envelope!";
								resultTextView.setText(result);
							}
							else if (d == 3.5) {
								cost = 1.06;
								result = "Cost = $" + Double.toString(cost);
								resultTextView.setText(result);

							}

							else {
								cost = .46 + (roundUp - 1) * .2;
								String rounded = formatter.format(cost);
								result = "Cost = $" + rounded;
								resultTextView.setText(result);
							}
						} else if (type == 2) {
							cost = .92 + (roundUp - 1) * .2;
							String rounded = formatter.format(cost);
							result = "Cost = $" + rounded;
							resultTextView.setText(result);

						} else if (type == 3) {
							if (d < 4)
								cost = 2.07;
							else
								cost = 2.07 + (roundUp - 3) * .17;

							String rounded = formatter.format(cost);
							result = "Cost = $" + rounded;
							resultTextView.setText(result);

						} else{ //if no radio button has been clicked
							result = "Please choose a shipping method.";
							resultTextView.setText(result);
							}

					}
				}

			}
		});

		// This section deals with the rules and restrictions tabs for shipping methods		
		TextView tv1 = (TextView) findViewById(R.id.tv1);
		TextView tv2 = (TextView) findViewById(R.id.tv2);
		TextView tv3 = (TextView) findViewById(R.id.tv3);
	
		tv1.setTextColor(Color.BLACK);
		tv1.setText("Letter Rules and Restrictions\n\n"
				+ "\t-Must be rectangular\n\n"
				+ "\t-Minimum size is 5\" long x 3 1/2\" high x 0.007\" thick\n\n"
				+ "\t-Maximum size is 11 1/2\" long x 6 1/8\" high x 1/4\" thick\n\n"
				+ "\t-Maximum weigth is 3.5 oz\n\n"
				+ "\t-A letter will be charged a nonmachinable surcharge if it�s a square"
				+ " letter 5\" x 5\" or larger, it doesn�t bend easily, has clasps or "
				+ "similar closure devices, has an address parallel to the shorter dimension"
				+ " of the letter, is lumpy, or the length divided by height is less than 1.3 or more than 2.5");
		tv2.setTextColor(Color.BLACK);
		tv2.setText("Large Envelope Rules and Restrictions\n\n"
				+ "\t-Must be rectangular.\n\n"
				+ "\t-Minimum size is either more than 11 1/2\" long x 6 1/8 \" high OR 1/4 \" thick\n\n"
				+ "\t-Maximum size is 15\" long x 12\" high x 3/4\" thick.\n\n"
				+ "\t-Maximum weight of 13 oz.\n\n"
				+ "\t-Large envelopes that exceed these dimensions will be charged package rates.");
		tv3.setTextColor(Color.BLACK);
		tv3.setText("Package Rules and Restrictions\n\n"
				+ "\t-Maximum weight of 13 oz.\n\n"
				+ "\t-Up to 108\" in combined length and girth.");
		
		

	}

	@Override
	protected void onSaveInstanceState (Bundle outState) {
	    super.onSaveInstanceState(outState);
	    outState.putString("result", result);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		result = savedInstanceState.getString("result");
		resultTextView.setText(result);
	}
	
	

}
