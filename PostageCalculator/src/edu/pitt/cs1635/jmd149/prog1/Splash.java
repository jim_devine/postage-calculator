package edu.pitt.cs1635.jmd149.prog1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class Splash extends Activity {

RelativeLayout rl; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		rl = (RelativeLayout)findViewById(R.id.relative_layout);
		rl.setBackgroundColor(Color.BLACK);
		TextView tv = (TextView)findViewById(R.id.splash_view);
		tv.setBackgroundColor(Color.GRAY);
		tv.setTextColor(Color.WHITE);
		
		
		Thread timer = new Thread(){
			
			public void run(){
				try{
					sleep(2000);
				}catch (InterruptedException e){
					e.printStackTrace();
					
				}finally{
					
					Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog1.MAINACTIVITY");
					startActivity(openStartingPoint);
					
				}
				
			}			
			
		};
		timer.start();		
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();		
		finish();
	}
	
	

}

